package com.rxq.angel.service;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.interfaceCase;
public interface caseService {
    httpBean addCase(interfaceCase interfaceCase);
    httpBean removeCase(interfaceCase interfaceCase);
    httpBean editCase(interfaceCase interfaceCase);
    httpBean getCaseAll();
}
