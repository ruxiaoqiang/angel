package com.rxq.angel.service;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.caseReport;
public interface caseReportService {
    httpBean addReport(caseReport caseReport);
    httpBean deleteReport(caseReport caseReport);
    httpBean updateReport(caseReport caseReport);
}
