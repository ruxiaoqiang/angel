package com.rxq.angel.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface IndexService {
     String getUser(Integer id);
     void updateShop(Map<String,Object> map);
     Integer getCount(Integer id);
     List<Map<String,Object>> getUrlList(Integer id);
}
