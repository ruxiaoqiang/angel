package com.rxq.angel.controller;

import com.rxq.angel.service.IndexService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import redis.clients.jedis.Jedis;
import com.rxq.angel.tool.redisBean;
@RestController
@Transactional
//允许跨域的注解
@CrossOrigin
public class HelloController {
    private final static Logger logger = LoggerFactory.getLogger(HelloController.class);
    @Resource
    private IndexService indexService;
    @RequestMapping(value = "index", method = RequestMethod.GET)
    @ApiOperation(value="index1号接口", notes="无参数请求")
    public String sayHello() {
        return "index";
    }

    //    post请求
    @ApiOperation(value = "post请求", notes = "测试post请求")
    @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "index1", method = RequestMethod.POST)
    public Map notSayHello(@RequestBody Map<String, Object> user) {
        Map<String, Object> map = new HashMap();
        map.put("key", user.get("name"));
        return map;
    }

    //  requestParam GET请求
    @ApiOperation(value = "GET请求", notes = "测试GET请求")
    @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "index2", method = RequestMethod.GET)
    public Map<String, Object> notSayHello1(@RequestParam(value = "name",required = false,defaultValue = "rxqdsg") String name) {
        Map<String, Object> map = new HashMap();
        map.put("key",name);
        return map;
    }

    @ApiOperation(value = "GET请求", notes = "测试GET请求")
    @ApiImplicitParam(name = "id", value = "姓名", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "index5", method = RequestMethod.GET)
    public Map<String, Object> notSayHello3(@RequestParam( value = "id",defaultValue = "-1",required = false) Integer id) {
        Map<String, Object> map = new HashMap();
        if (id==-1){
            id=null;
        }
        List<Map<String,Object>> list = indexService.getUrlList(id);
        for (Map<String,Object> map1:list ){
            map.put((String) map1.get("name"),map1.get("url"));
        }
        return map;
    }

    @ApiOperation(value = "GET请求", notes = "测试GET请求")
    @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "String", paramType = "path")
    @RequestMapping(value = "index3", method = RequestMethod.GET)
    public Map<String, Object> notSayHello2(@RequestParam("name") String name) {
        Map<String, Object> map = new HashMap();
        Map<String,Object> param = new HashMap<>();
        Jedis jedis = redisBean.getRedisBean();
//        分布式锁
        if (jedis.get("rxq")==null){
            jedis.setex("rxq",2,"rxq");
            Integer count = indexService.getCount(1);
            if (count>0){
                count--;
                param.put("amount",count);
                indexService.updateShop(param);
                map.put("key", name);
                map.put("key1", "剩余货物为"+count);
            }else {
                map.put("key","无货");
            }

        }else {
            map.put("key","获取key失败请重试");
        }
        return map;
    }
}
