package com.rxq.angel.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.concurrent.*;
import com.rxq.angel.tool.toolUntil;
import com.rxq.angel.service.caseService;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.interfaceCase;
import com.rxq.angel.service.caseReportService;
import com.rxq.angel.bean.caseReport;
import com.rxq.angel.tool.HttpRequest;
@RestController
@Transactional
@RequestMapping(value = "auto")
public class TaskController {
    @Autowired
    private caseService caseService;
    @Autowired
    private caseReportService caseReportService;
    @GetMapping(value = "executor")
    public  String autoCaseExc(@RequestParam(value = "taskId") Integer taskId ){
        String ResCode;
        CopyOnWriteArrayList<caseReport> resList = new CopyOnWriteArrayList();
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        try {
            httpBean httpBean = caseService.getCaseAll();
            List<interfaceCase> caseList = (List<interfaceCase>) httpBean.getData();
            CountDownLatch countDownLatch =new CountDownLatch(caseList.size());
            for (int i=0;i<caseList.size();i++){
                executorService.execute(new AutoCase(caseList.get(i),countDownLatch,resList,taskId));
            }
            countDownLatch.await();
            for (caseReport caseReport:resList){
                caseReportService.addReport(caseReport);
                System.out.println(caseReport.getCaseId());
                System.out.println(caseReport.getState());
            }
            ResCode= "true";
        }catch (Exception e){
            ResCode="false";
            e.printStackTrace();
        }finally {
            executorService.shutdown();
        }
        return ResCode;
    }
}

class AutoCase implements Runnable{
    interfaceCase interfaceCase;
    CountDownLatch countDownLatch;
    CopyOnWriteArrayList copyOnWriteArrayList;
    caseReport caseReport = new caseReport();
    Integer taskId;
    public AutoCase(interfaceCase interfaceCase, CountDownLatch countDownLatch,CopyOnWriteArrayList copyOnWriteArrayList,Integer taskId){
        this.interfaceCase =interfaceCase;
        this.countDownLatch = countDownLatch;
        this.copyOnWriteArrayList = copyOnWriteArrayList;
        this.taskId =taskId;
    }
    @Override
    public void run() {
//        调用接口对象发送请求 xxx.sendpost()
        Long startTime = toolUntil.getTimestamp();
        httpBean httpBean = new httpBean();
        try {
            if ("POST".equals(interfaceCase.getRequestMethod())){
                httpBean =HttpRequest.sendPost(interfaceCase);
            }else {
                httpBean =HttpRequest.sendGet(interfaceCase);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        caseReport.setCaseId(interfaceCase.getCaseId());
        caseReport.setState(21);
        Long endTime = toolUntil.getTimestamp();
        caseReport.setExecTime(String.valueOf(endTime-startTime));
        caseReport.setMessage(String.valueOf(httpBean.getCode()));
        caseReport.setTaskId(taskId);
        copyOnWriteArrayList.add(caseReport);
        countDownLatch.countDown();
    }
}
