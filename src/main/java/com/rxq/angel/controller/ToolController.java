package com.rxq.angel.controller;

import com.rxq.angel.service.IndexService;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.rxq.angel.tool.redisBean;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
//@Transactional
@RestController
public class ToolController {
    @Resource
    private IndexService indexService;
    @RequestMapping(value = "time",method = RequestMethod.GET)
    public String getTimeTrap(@Param("time") String time){
        if ("".equals(time)){
            Date date = new Date();
            time = date.toString();
        }else {
            return time;
        }
        String name = indexService.getUser(1);
        return  name;

    }

    @RequestMapping(value = "redis",method = RequestMethod.GET)
    public String redisTest(@RequestParam("key") String key){
        assert true;
        Jedis jedis = redisBean.getRedisBean();
        if (!jedis.exists(key)){
            jedis.set(key,"RXQ");
        }
        Set<String> set = jedis.keys("*");
        System.out.println(jedis.get(key));
        Map<String,Object> map = new HashMap<>();
        map.put("amount",10);
        indexService.updateShop(map);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                indexService.updateShop(map);
//            }
//        }).start();
//        String a  = "a";
//        Integer.valueOf(a);
        return jedis.get(key);
    }
}
