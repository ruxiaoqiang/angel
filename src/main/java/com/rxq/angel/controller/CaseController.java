package com.rxq.angel.controller;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.interfaceCase;
import com.rxq.angel.service.caseService;
import com.rxq.angel.service.caseLifeService;
import com.rxq.angel.bean.caseLife;
import com.rxq.angel.tool.ConstValue;
import com.rxq.angel.tool.toolUntil;
@RestController
@Transactional
@RequestMapping(value = "case")
public class CaseController {
    @Autowired
    private caseService caseService;
    @Autowired
    private caseLifeService caseLifeService;
    @PostMapping(value = "addCase")
//    @ApiOperation(value = "MESSAGE", notes = "添加测试用例")
//    @ApiImplicitParam(name = "param", value = "姓名", required = true, dataType = , paramType = "interfaceCase")
    public httpBean addCase(@RequestBody interfaceCase interfaceCase){
        httpBean httpBean = new httpBean();
        caseLife caseLife = new caseLife();
        caseLife.setCaseId(interfaceCase.getCaseId());
        caseLife.setState(ConstValue.caseLifeAdd);
        caseLife.setCreate_time(String.valueOf(toolUntil.getTimestamp()));
        caseService.addCase(interfaceCase);
        caseLifeService.addCaseLife(caseLife);
        return httpBean;
    }
    @PostMapping(value = "editCase")
    public httpBean editCase(@RequestBody interfaceCase interfaceCase){
        httpBean httpBean = new httpBean();
        caseService.editCase(interfaceCase);
        caseLife caseLife = new caseLife();
        caseLife.setCaseId(interfaceCase.getCaseId());
        caseLife.setState(ConstValue.caseLifeUpdate);
        caseLife.setUpdate_time(String.valueOf(toolUntil.getTimestamp()));
        caseLife.setDataValue(JSON.toJSONString(interfaceCase));
        caseLifeService.addCaseLife(caseLife);

        return httpBean;
    }
    @PostMapping(value = "removeCase")
    public httpBean removeCase(@RequestBody interfaceCase interfaceCase){
        httpBean httpBean = new httpBean();
        httpBean.setCode(200);
        caseService.removeCase(interfaceCase);
        caseLife caseLife = new caseLife();
        caseLife.setCaseId(interfaceCase.getCaseId());
        caseLife.setState(ConstValue.caseLifeDelete);
        caseLife.setUpdate_time(String.valueOf(toolUntil.getTimestamp()));
        caseLifeService.addCaseLife(caseLife);
        return httpBean;
    }
    @GetMapping(value = "getCase")
    public httpBean getInterfaCecase(@RequestBody interfaceCase interfaceCase){
        httpBean httpBean = caseService.getCaseAll();
        return httpBean;
    }

}
