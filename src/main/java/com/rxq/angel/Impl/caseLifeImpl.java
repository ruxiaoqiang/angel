package com.rxq.angel.Impl;
import com.rxq.angel.bean.caseLife;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.service.caseLifeService;
import org.springframework.stereotype.Service;
import com.rxq.angel.mapper.caseLifeMapper;

import javax.annotation.Resource;

@Service
public class caseLifeImpl implements caseLifeService  {
    @Resource
    private caseLifeMapper caseLifeMapper;
    @Override
    public httpBean getCaseLife() {
        caseLifeMapper.getCaseLifeAll();
        return null;
    }

    @Override
    public httpBean addCaseLife(caseLife caseLife) {
        caseLifeMapper.addCaseLife(caseLife);
        return  new httpBean();
    }
}
