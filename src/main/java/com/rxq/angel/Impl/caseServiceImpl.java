package com.rxq.angel.Impl;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.interfaceCase;
import com.rxq.angel.service.caseService;
import com.rxq.angel.mapper.interfaceCaseMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class caseServiceImpl implements caseService  {
    @Resource
    interfaceCaseMapper interfaceCaseMapper;
    @Override
    public httpBean addCase(interfaceCase interfaceCase) {
        interfaceCaseMapper.addCase(interfaceCase);
        return null;
    }

    @Override
    public httpBean removeCase(interfaceCase interfaceCase) {
        interfaceCaseMapper.removeCase(interfaceCase);
        return null;
    }

    @Override
    public httpBean editCase(interfaceCase interfaceCase) {
        interfaceCaseMapper.updateCase(interfaceCase);
        return null;
    }

    @Override
    public httpBean getCaseAll() {
        httpBean httpBean = new httpBean();
        List<interfaceCase> list =interfaceCaseMapper.getCaseAll();
        Map<String,Object> map = new HashMap<>();
        httpBean.setData(list);
        return httpBean;
    }
}
