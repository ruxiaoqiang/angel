package com.rxq.angel.Impl;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.service.caseReportService;
import org.springframework.stereotype.Service;
import com.rxq.angel.mapper.caseReportMapper;
import com.rxq.angel.bean.caseReport;

import javax.annotation.Resource;

@Service
public class caseReportImpl implements caseReportService {
    @Resource
    private caseReportMapper caseReportMapper ;
    @Override
    public httpBean addReport(caseReport caseReport) {
        caseReportMapper.addCaseReport(caseReport);
        return null;
    }

    @Override
    public httpBean deleteReport(caseReport caseReport) {
        caseReportMapper.updateCaseReport(caseReport);
        return null;
    }

    @Override
    public httpBean updateReport(caseReport caseReport) {
        caseReportMapper.updateCaseReport(caseReport);
        return null;
    }
}
