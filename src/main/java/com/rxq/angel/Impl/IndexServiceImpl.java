package com.rxq.angel.Impl;

import com.rxq.angel.service.IndexService;
import com.rxq.angel.mapper.index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class IndexServiceImpl implements IndexService {
    @Resource
    private index indexMapper;
    @Override
    public String getUser(Integer id){
        String name = indexMapper.getuser(id);
        List resMap = indexMapper.userList();
        for (int i=0;i<resMap.size();i++){
            System.out.println(resMap.get(i));
        }
        Map map = new HashMap();
        map.put("id",4);
        map.put("name","rxq");
        map.put("username","rxq");
        map.put("pass","asdf");
        indexMapper.insertUser(map);
        return name;
    }

    @Override
    public void updateShop(Map<String, Object> map) {
        indexMapper.updateShop(map);

    }

    @Override
    public Integer getCount(Integer id) {
        return indexMapper.getAmount(id);
    }

    @Override
    public List<Map<String, Object>> getUrlList(Integer id) {

        return indexMapper.getUrlList(id);
    }
}
