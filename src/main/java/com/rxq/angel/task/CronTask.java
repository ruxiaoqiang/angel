package com.rxq.angel.task;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CronTask {
//    fixedRate 分钟
    @Scheduled(fixedRate = 300000)
    public void fixedRateJob() {
        System.out.println("fixedRate 每隔3秒" + new Date());
    }
}

