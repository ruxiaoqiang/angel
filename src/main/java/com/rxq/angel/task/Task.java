package com.rxq.angel.task;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.ArrayList;
public class Task {
    public static ArrayList<String> getPermutation1(String A) {
        int n = A.length();
        ArrayList<String> res = new ArrayList<>();
        res.add(String.valueOf(A.charAt(0)));// 初始化,包含第一个字符
        for (int i = 1; i < n; i++) {// 第二个字符插入到前面生成集合的每个元素里面
            ArrayList<String> res_new = new ArrayList<>();
            char c = A.charAt(i);// 新字符
            for (String str : res) {// 访问上一趟集合中的每个字符串
                // 插入到每个位置,形成一个新串
                String newStr = c + str;// 加在前面
                res_new.add(newStr);
                newStr = str + c;// 加在后面
                res_new.add(newStr);
                // 加在中间
                for (int j = 1; j < str.length(); j++) {
                    newStr = str.substring(0, j) + c + str.substring(j);
                    res_new.add(newStr);
                }
            }
            res = res_new;// 更新

        }
        return res;
    }
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        TaskOne taskOne = new TaskOne(0,50001,countDownLatch);
        TaskOne tasktwo = new TaskOne(50001,100000,countDownLatch);
        executorService.execute(taskOne);
//        没有返回值
        executorService.execute(tasktwo);
        countDownLatch.await();
        long sum = Long.valueOf(taskOne.sum)+Long.valueOf(tasktwo.sum);
        executorService.shutdownNow();
        ArrayList list = getPermutation1("abc");
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }
}
class TaskOne implements Runnable{
    int start;
    int end ;
    int sum ;
    CountDownLatch n;
    public TaskOne(int start,int end,CountDownLatch n){
        this.start =start;
        this.end =end;
        this.n = n;
    }

    @Override
    public void run() {
        for (int i=start;i<end;i++){
            sum+=i;
        }
        n.countDown();
    }
}
