package com.rxq.angel.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import com.rxq.angel.bean.httpBean;
import com.rxq.angel.bean.caseReport;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface caseReportMapper {
    @Insert("insert into caseReport(caseId,taskID,state,execTime,message) values(#{caseId},#{taskID},#{state},#{execTime},#{message})")
    void  addCaseReport(caseReport caseReport);
    @Update("update caseReport set caseId =#{caseId},taskId=#{taskID},state=#{state},execTime=#{execTime},message =#{message} where id = #{id}")
    httpBean updateCaseReport(caseReport caseReport);
}
