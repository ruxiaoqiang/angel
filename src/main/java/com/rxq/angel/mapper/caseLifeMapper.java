package com.rxq.angel.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import com.rxq.angel.bean.caseLife;
import com.rxq.angel.bean.httpBean;
@Mapper
public interface caseLifeMapper {
    @Insert("insert into caseLife(caseId,create_time,update_time,dataValue,state) values(#{caseId},#{create_time},#{update_time},#{dataValue},#{state})")
    void addCaseLife(caseLife caseLife);
    httpBean getCaseLifeAll();
}
