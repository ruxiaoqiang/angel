package com.rxq.angel.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import com.rxq.angel.bean.interfaceCase;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface interfaceCaseMapper {
    void addCase(interfaceCase interfaceCase);
    @Delete("delete from interfaceCase where caseId = #{caseId,jdbcType=INTEGER} ")
    void removeCase(interfaceCase interfaceCase);
    void updateCase(interfaceCase interfaceCase);
    @Select("select * from interfaceCase")
    List<interfaceCase> getCaseAll();
    List<interfaceCase> getCaseBystate(interfaceCase interfaceCase);
}
