package com.rxq.angel.mapper;

import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
public interface index {
    @Select("select name from user where id = #{id}")
    String getuser(Integer id);
    ArrayList<Map<String,Object>> userList();
    void insertUser(Map<String,Object> map);
    void updateShop(Map<String,Object> map);
    @Select("select amount from shangpin where id = #{id}")
    Integer getAmount(Integer id);
    @Select(
            "<script> select * from view where 1=1 " +
                    "<if test = 'id !=null' > and id = #{id} </if> "+
                    " </script>"
    )
    List<Map<String,Object>> getUrlList(Integer id);
//    根据ID获取结果
}
