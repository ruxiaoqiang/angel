package com.rxq.angel.tool;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class aopTest {
    @Pointcut("execution(* com.rxq.angel.Impl.*.*(..))")
    public void pointcut() {
        System.out.println(888);
    }
    @Before("pointcut()")
    public void doBefore(){
        System.out.println("执行切面开始");
    }
}
