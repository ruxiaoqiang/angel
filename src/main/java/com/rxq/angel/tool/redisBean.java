package com.rxq.angel.tool;
import redis.clients.jedis.Jedis;
//静态内部类单例
public class redisBean {
    private redisBean(){
    }
    private static class innerclass{
        private static Jedis jedis = new Jedis("localhost");
    }
    public static Jedis getRedisBean(){
        return innerclass.jedis;
    }
}
