package com.rxq.angel.publicTool;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "tool")
public class tool {
    @RequestMapping(value = "getTime",method = RequestMethod.GET)
    public Long getTime(){
        long time  = System.currentTimeMillis();
        return time;
    }
}
