# 使用rxq基础镜像
FROM angel/ruxiaoqiang:v1.0
#制作者
MAINTAINER ruxiaoqiang
#环境变量
#ENV rxq=shuai zyq=chou
#环境变量 仅在build时候有效
#ARG test=001
# 将war包放在home下
ADD  /target/*.jar /app.jar
# 拷贝复制仅在当前机器中，不可跨机器上
#COPY a.sh /home/
#构建时执行的命令
#RUN mkdir temppp & echo $rxq >> /home/b.txt
#挂载目录
#VOLUME ['/Users/ruxiaoqiang/Rxq']
#镜像运行时候执行 多个CMD仅最后一个生效
#CMD ['sh /home/a.sh']
#暴露的端口 -P 运行时随机指定
#EXPOSE 18000 19878
#当以此次构建的镜像为基础镜像时执行此命令
#ONBUILD RUN echo $test >> /home/c.txt
ENTRYPOINT ["sh","-c","java  -jar /app.jar "]
# docker run -d 后台运行

# docker run -v 目录映射  /home：/home

# docker run -p 端口映射 9000：8000

# docker run -e 环境变量 变量name=变量值





